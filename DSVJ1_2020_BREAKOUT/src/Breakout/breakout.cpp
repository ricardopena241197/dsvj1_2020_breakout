#include "breakout.h"
#include "AuxGlobales/aux_globales.h"
#include "Ventana/ventana.h" 
#include "Gameplay/juego.h"
#include "Menus/MenuPrincipal/menu_principal.h"
#include "Menus/MenuJuegoFinalizado/menu_juego_finalizado.h"
#include "Menus/MenuPausa/menu_pausa.h"
#include "Menus/MenuCreditos/menu_creditos.h"
#include "Menus/MenuOpciones/menu_opciones.h"
#include "Sonidos/musica.h"

#include "Texturas/texturas.h"

#include <iostream>
#include <time.h>

namespace breakout
{
	Escenas escena = Escenas::MenuPrincipal;

	void init()
	{
		srand(time(NULL));
		ventana::abrirVentana();
		musica::init();	
		InitAudioDevice();
	}

	void update()
	{
		switch (escena)
		{
		case Escenas::MenuPrincipal:
			menu_principal::menu();
			break;
		case Escenas::Jugar:
			juego::jugar();
			break;
		case Escenas::Opciones:
			menu_opciones::menu();
			break;
		case Escenas::Creditos:
			menu_creditos::menu();
			break;
		case Escenas::JuegoFinalizado:
			menu_juego_finalizado::menu();
			break;
		case Escenas::Pausa:
			menu_pausa::menu();
			break;
		default:
			break;
		}
	}

	void draw()
	{
		BeginDrawing();

		switch (escena)
		{
		case Escenas::MenuPrincipal:
			menu_principal::draw();
			break;
		case Escenas::Jugar:
			juego::draw();
			break;
		case Escenas::Opciones:
			menu_opciones::draw();
			break;
		case Escenas::Creditos:
			menu_creditos::draw();
			break;
		case Escenas::JuegoFinalizado:
			menu_juego_finalizado::draw();
			break;
		case Escenas::Pausa:
			menu_pausa::draw();
			break;
		default:
			break;
		}
		EndDrawing();
	}

	void deinit()
	{
		CloseAudioDevice();
	}

	void juego()
	{
		init();

		while (!WindowShouldClose() && !aux_globales::aux_salida)
		{
			update();
			draw();
		}

		deinit();
	}
}

