#ifndef BREAKOUT_H
#define BREAKOUT_H

#include "raylib.h"
enum class Escenas
{
	MenuPrincipal,
	Jugar,
	Opciones, 
	Creditos,
	JuegoFinalizado,
	Pausa
};

namespace breakout
{
	extern Escenas escena;
	void juego();
}


#endif // !BREAKOUT_H

