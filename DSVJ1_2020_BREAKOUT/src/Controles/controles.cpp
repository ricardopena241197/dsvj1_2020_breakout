#include "controles.h"

namespace controles
{
	int moverArriba = KEY_UP;
	int moverAbajo = KEY_DOWN;
	int moverIzquierda = KEY_LEFT;
	int moverDerecha = KEY_RIGHT;
	int pausa = KEY_P;
	int aceptar = KEY_ENTER;
	int usar = KEY_SPACE;
}