#ifndef CONTROLES_H
#define CONTROLES_H
#include "raylib.h"

namespace controles
{
	extern int moverArriba;
	extern int moverAbajo;
	extern int moverIzquierda;
	extern int moverDerecha;
	extern int pausa;
	extern int aceptar;
	extern int usar;
}

#endif // !CONTROLES_H

