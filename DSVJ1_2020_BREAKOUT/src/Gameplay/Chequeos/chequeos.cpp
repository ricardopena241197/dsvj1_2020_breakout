#include "chequeos.h"

namespace chequeos
{
	bool chequeoNivelCompletado(Bloques bloques[juego::cantFilasBloques][juego::cantColumnasBloques])
	{
		for (short i = 0; i < juego::cantFilasBloques; i++)
		{
			for (short j = 0; j < juego::cantColumnasBloques; j++)
			{
				if (bloques[i][j].activo)
				{
					return false;
				}
			}
		}
		return true;
	}

	bool chequeoJuegoFinalizado(bool nivelesCompletados[cantNiveles])
	{
		for (short i = 0; i < cantNiveles; i++)
		{
			if (!nivelesCompletados[i])
			{
				return false;
			}
		}
		return true;
	}	

	bool chequeoJuegoFinalizado(int vidas)
	{		
		return (vidas <=0);
	}
}