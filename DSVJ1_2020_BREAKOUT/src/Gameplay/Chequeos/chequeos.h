#ifndef CHEQUEOS_H
#define CHEQUEOS_H

#include "Gameplay/juego.h"

namespace chequeos
{
	bool chequeoNivelCompletado(Bloques bloques[juego::cantFilasBloques][juego::cantColumnasBloques]);
	bool chequeoJuegoFinalizado(bool nivelesCompletados[cantNiveles]);
	bool chequeoJuegoFinalizado(int vidas);
}


#endif // !CHEQUEOS_H

