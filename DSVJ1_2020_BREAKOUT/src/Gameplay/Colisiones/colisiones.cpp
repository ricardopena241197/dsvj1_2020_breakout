#include <iostream>
#include <time.h>
#include "colisiones.h"
#include "Ventana/ventana.h"

namespace colisiones
{
	namespace col_pelota
	{
		bool colisionPelotaLadoDer(Pelota pelota)
		{
			return (pelota.posicion.x + pelota.radio >= ventana::anchoVentana);
		}

		bool colisionPelotaLadoIzq(Pelota pelota)
		{
			return (pelota.posicion.x - pelota.radio <= 0);
		}

		bool colisionPelotaLadoSup(Pelota pelota)
		{
			return (pelota.posicion.y - pelota.radio <= 0);
		}

		bool colisionPelotaLadoInf(Pelota pelota,Paletas paleta)
		{
			return (pelota.posicion.y + pelota.radio >= paleta.figura.y+paleta.figura.height*2);
		}

		bool colisionPelotaPaleta(Pelota pelota, Paletas paleta)
		{
			return CheckCollisionCircleRec(pelota.posicion, pelota.radio, paleta.figura);
		}
	}

	namespace col_paleta
	{
		bool colisionPaletaLadoIzq(Paletas paleta)
		{
			return (paleta.figura.x <= 0);
		}

		bool colisionPaletaLadoDer(Paletas paleta)
		{
			return (paleta.figura.x + paleta.figura.width >= ventana::anchoVentana);
		}
	}
	
	namespace col_bloques
	{		
		void calculoLadoColision(Pelota& pelota, Bloques bloque)
		{
			if (pelota.posicion.y >= bloque.figura.y + bloque.figura.height || pelota.posicion.y <= bloque.figura.y)
			{
				pelota.velocidad.y *= -1;
			}

			if (pelota.posicion.x >= bloque.figura.x+bloque.figura.width || pelota.posicion.x <= bloque.figura.x )
			{
				pelota.velocidad.x *= -1;
			}				
		}

		void colisionBloquePelota(Bloques bloques[juego::cantFilasBloques][juego::cantColumnasBloques], Pelota & pelota,int &puntaje)
		{
			for (short i = 0; i < juego::cantFilasBloques; i++)
			{
				for (short j = 0; j < juego::cantColumnasBloques; j++)
				{
					if (bloques[i][j].activo)
					{
						if (!pelota.colisionando)
						{
							if (CheckCollisionCircleRec(pelota.posicion, pelota.radio, bloques[i][j].figura))
							{
								if (bloques[i][j].vida > 0)
								{
									bloques[i][j].vida -= 1;
									if (bloques[i][j].vida <= 0)
									{
										bloques[i][j].activo = false;
										juego::actualizar::calculoDePuntaje(puntaje, bloques[i][j].tipoDeBloque);
									}
								}
								pelota.colisionando = true;
								calculoLadoColision(pelota, bloques[i][j]);
							}
						}
						else
						{
							if (CheckCollisionCircleRec(pelota.posicion, pelota.radio, bloques[i][j].figura))
							{								
								pelota.colisionando = true;
								i = juego::cantFilasBloques;
								j = juego::cantColumnasBloques;
							}
							else
							{
								pelota.colisionando = false;
							}
						}
						
					}
				}
			}
					
		}
	}
}
