#ifndef COLISIONES_H
#define COLISIONES_H

#include "Gameplay/juego.h"

namespace colisiones
{
	namespace col_pelota
	{
		bool colisionPelotaLadoDer(Pelota pelota);
		bool colisionPelotaLadoIzq(Pelota pelota);
		bool colisionPelotaLadoSup(Pelota pelota);
		bool colisionPelotaLadoInf(Pelota pelota, Paletas paleta);
		bool colisionPelotaPaleta(Pelota pelota, Paletas paleta);
	}

	namespace col_paleta
	{
		bool colisionPaletaLadoIzq(Paletas paleta);
		bool colisionPaletaLadoDer(Paletas paleta);
	}
	
	namespace col_bloques
	{		
		void colisionBloquePelota(Bloques bloques[juego::cantFilasBloques][juego::cantColumnasBloques], Pelota & pelota, int& puntaje);		
	}
	
}

#endif // !COLISIONES_H

