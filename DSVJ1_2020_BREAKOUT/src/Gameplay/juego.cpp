#include "Breakout/breakout.h"
#include "juego.h"
#include "Controles/controles.h"
#include "Gameplay/Colisiones/colisiones.h"
#include "Gameplay/Chequeos/chequeos.h"
#include "Sonidos/musica.h"

namespace juego
{
	bool juegoInicializado = false;
	Bloques bloques[cantFilasBloques][cantColumnasBloques];
	namespace bloque
	{
		Texture2D bloqueFacil;
		Texture2D bloqueMedio;
		Texture2D bloqueDificil;
	}	

	namespace vidas
	{
		Texture2D vidas;
	}

	Jugador jugador1;
	Pelota pelota[cantMaxPelotas];
	Fondo fondo;	
		
	namespace inicializar
	{
		void inicializarPaleta()
		{
			using namespace paleta;
			using namespace ventana;

			jugador1.paleta.figura.height = alto;
			jugador1.paleta.figura.width = ancho;

			jugador1.paleta.figura.x = anchoVentana / 2 - jugador1.paleta.figura.width / 2;
			jugador1.paleta.figura.y = altoVentana - jugador1.paleta.figura.height * 4;

			jugador1.paleta.color = WHITE;

			jugador1.paleta.velocidad = velocidad;

			jugador1.paleta.textura = LoadTexture(texturas::juego::rutaPaleta);
			jugador1.paleta.textura.height = jugador1.paleta.figura.height;
			jugador1.paleta.textura.width = jugador1.paleta.figura.width;

		}

		void inicializarJugador()
		{
			jugador1.nivel = 1;
			jugador1.poder = Poderes::Ninguno;
			jugador1.puntaje = 0;
			jugador1.vidas = 5;

			for (short i = 0; i < cantNiveles; i++)
			{
				jugador1.nivelesCompletados[i] = false;
			}

			inicializarPaleta();
		}

		void inicializarPelota()
		{
			pelota[0].radio = 5.0f;
			pelota[0].color = WHITE;
			pelota[0].enMovimiento = false;
			pelota[0].colisionando = false;
			pelota[0].velocidad.x = 5;
			pelota[0].velocidad.y = -5;
			pelota[0].posicion.x = jugador1.paleta.figura.x + jugador1.paleta.figura.width / 2;
			pelota[0].posicion.y = jugador1.paleta.figura.y - pelota[0].radio - 2;

			pelota[0].textura = LoadTexture(texturas::juego::rutaPelota);
			pelota[0].textura.height = pelota[0].radio * 2;
			pelota[0].textura.width = pelota[0].radio * 2;
		}

		void inicializarPelota(Pelota & pelota)
		{
			pelota.radio = 5.0f;
			pelota.color = WHITE;
			pelota.enMovimiento = false;
			pelota.colisionando = false;
			pelota.velocidad.x = 5;
			pelota.velocidad.y = -5;
			pelota.posicion.x = jugador1.paleta.figura.x + jugador1.paleta.figura.width / 2;
			pelota.posicion.y = jugador1.paleta.figura.y - pelota.radio - 2;
		}		

		void inicializarBloques()
		{
			using namespace bloque;

			bloqueFacil = LoadTexture(texturas::juego::rutaBloqueFacil);
			bloqueFacil.height = alto;
			bloqueFacil.width = ancho;
			bloqueMedio = LoadTexture(texturas::juego::rutaBloqueMedio);
			bloqueMedio.height = alto;
			bloqueMedio.width = ancho;
			bloqueDificil = LoadTexture(texturas::juego::rutaBloqueDificil);
			bloqueDificil.height = alto;
			bloqueDificil.width = ancho;

			int auxPosY = 0;
			Color auxColor = WHITE;

			for (short i = 0; i < cantFilasBloques; i++)
			{
				bloques[i][0].color = auxColor;
				bloques[i][0].vida = 1;
				bloques[i][0].activo = true;
				bloques[i][0].tipoDeBloque = TipoBloque::Facil;
				bloques[i][0].figura.width = ancho;
				bloques[i][0].figura.height = alto;
				bloques[i][0].figura.y = auxPosY;
				bloques[i][0].figura.x = 0;

				for (short j = 1; j < cantColumnasBloques; j++)
				{
					bloques[i][j].color = auxColor;
					bloques[i][j].vida = 1;
					bloques[i][j].activo = true;
					bloques[i][j].tipoDeBloque = TipoBloque::Facil;
					bloques[i][j].figura.width = ancho;
					bloques[i][j].figura.height = alto;
					bloques[i][j].figura.y = auxPosY;
					bloques[i][j].figura.x = bloques[i][j - 1].figura.x + ancho;
				}
				auxPosY += alto;
			}				
		}

		void inicializarFondo()
		{
			fondo.pos.x = 0;
			fondo.pos.y = 0;
			fondo.color = WHITE;
			fondo.textura = LoadTexture(texturas::juego::rutaFondo);
			fondo.textura.height = ventana::altoVentana;
			fondo.textura.width = ventana::anchoVentana;

			vidas::vidas= LoadTexture(texturas::juego::rutaVidas);
			vidas::vidas.height=20;
			vidas::vidas.width=20;
		}
	}

	namespace dibujar
	{
		void dibujarPaleta()
		{
			DrawTexture(jugador1.paleta.textura, jugador1.paleta.figura.x, jugador1.paleta.figura.y, jugador1.paleta.color);

			#if DEBUG
			DrawRectangleLinesEx(jugador1.paleta.figura, 1, GREEN);
			#endif // !DEBUG

		}

		void dibujarPelota()
		{
			Vector2 auxPos;
			auxPos.x = pelota[0].posicion.x - pelota[0].radio;
			auxPos.y = pelota[0].posicion.y - pelota[0].radio;
			DrawTexture(pelota[0].textura, auxPos.x, auxPos.y, pelota[0].color);

			#if DEBUG
			DrawRectangleLines(auxPos.x, auxPos.y, pelota[0].radio*2, pelota[0].radio*2, GREEN);
			#endif // !DEBUG
		}

		void dibujarBloques()
		{
			using namespace bloque;
			for (short i = 0; i < cantFilasBloques; i++)
			{
				for (short j = 0; j < cantColumnasBloques; j++)
				{
					if (bloques[i][j].activo)
					{
						
						switch (bloques[i][j].vida)
						{
						case 1:
							DrawTexture(bloqueFacil, bloques[i][j].figura.x, bloques[i][j].figura.y, bloques[i][j].color);
							break;
						case 2:
							DrawTexture(bloqueMedio, bloques[i][j].figura.x, bloques[i][j].figura.y, bloques[i][j].color);
							break;
						case 3:
							DrawTexture(bloqueDificil, bloques[i][j].figura.x, bloques[i][j].figura.y, bloques[i][j].color);
							break;
						default:
							break;
						}

						#if DEBUG
						DrawRectangleLinesEx(bloques[i][j].figura, 1, GREEN);
						#endif // !DEBUG
					}
				}
			}
		}

		void dibujarFondo()
		{
			DrawTexture(fondo.textura, fondo.pos.x, fondo.pos.y, fondo.color);
		}

		void dibujarPuntaje()
		{
			int tamLetraPuntaje = 20;
			Vector2 posPuntaje;
			Color colorPuntaje = WHITE;
			posPuntaje.x = 0;
			posPuntaje.y = ventana::altoVentana - tamLetraPuntaje;
			DrawText(TextFormat("Puntaje: %i",jugador1.puntaje), posPuntaje.x, posPuntaje.y, tamLetraPuntaje,colorPuntaje);
		}

		void dibujarNivel()
		{
			int tamLetraNivel = 20;
			Vector2 posNivel;
			Color colorNivel = WHITE;
			posNivel.x = ventana::anchoVentana-MeasureText(TextFormat("Nivel: %i", jugador1.nivel), tamLetraNivel)-10;
			posNivel.y = ventana::altoVentana - tamLetraNivel;
			DrawText(TextFormat("Nivel: %i", jugador1.nivel),posNivel.x,posNivel.y, tamLetraNivel, colorNivel);
		}

		void dibujarVidas()
		{
			int tamLetraVidas = 20;
			Vector2 posVidas;
			Color colorVidas = WHITE;
			posVidas.x = ventana::anchoVentana/2 -((vidas::vidas.width/2)*jugador1.vidas/2)- MeasureText("Vidas: ", tamLetraVidas);
			posVidas.y = ventana::altoVentana - tamLetraVidas;
			DrawText("Vidas: ", posVidas.x, posVidas.y, tamLetraVidas, colorVidas);
			int auxX = posVidas.x + MeasureText("Vidas: ", tamLetraVidas);
			if (jugador1.vidas>0)
			{
				DrawTexture(vidas::vidas, auxX, posVidas.y, colorVidas);
				for (short i = 1; i < jugador1.vidas; i++)
				{
					auxX += vidas::vidas.width;
					DrawTexture(vidas::vidas, auxX, posVidas.y, colorVidas);
				}
			}			
		}
	}

	namespace desinicializar
	{
		void desinicializarTexturas()
		{
			UnloadTexture(jugador1.paleta.textura);
			UnloadTexture(pelota[0].textura);
			UnloadTexture(fondo.textura);

			UnloadTexture(bloque::bloqueFacil);
			UnloadTexture(bloque::bloqueMedio);
			UnloadTexture(bloque::bloqueDificil);
			UnloadTexture(vidas::vidas);
		}
	}

	namespace actualizar
	{
		void finalizarPartida()
		{	
			inicializar::inicializarPelota(pelota[0]);				
			breakout::escena = Escenas::JuegoFinalizado;			
		}

		void imput()
		{
			using namespace controles;
			using namespace colisiones;
			using namespace col_paleta;

			if (IsKeyDown(moverDerecha))
			{
				if (!colisionPaletaLadoDer(jugador1.paleta))
				{
					jugador1.paleta.figura.x += jugador1.paleta.velocidad;
				}
			}

			if (IsKeyDown(moverIzquierda))
			{
				if (!colisionPaletaLadoIzq(jugador1.paleta))
				{
					jugador1.paleta.figura.x -= jugador1.paleta.velocidad;
				}
			}

			if (IsKeyDown(usar))
			{
				if (!pelota[0].enMovimiento)
				{
					pelota[0].enMovimiento = true;
				}
			}

			if (IsKeyDown(pausa))
			{
				breakout::escena = Escenas::Pausa;
			}
			
		}

		void movimientoPelota(Pelota &pelotas)
		{
			using namespace colisiones;
			using namespace col_pelota;
			using namespace col_bloques;

			if (pelotas.enMovimiento)
			{
				if (!pelotas.colisionando)
				{
					if (colisionPelotaLadoInf(pelotas,jugador1.paleta))
					{
						inicializar::inicializarPelota(pelotas);
						pelotas.velocidad.y *= -1;
						jugador1.vidas--;
						if (chequeos::chequeoJuegoFinalizado(jugador1.vidas))
						{
							finalizarPartida();
						}						
						pelotas.colisionando = true;
					}

					if (colisionPelotaLadoSup(pelotas))
					{
						pelotas.velocidad.y *= -1;
						pelotas.colisionando = true;
					}

					if (colisionPelotaLadoDer(pelotas) || colisionPelotaLadoIzq(pelotas))
					{
						pelotas.velocidad.x *= -1;
						pelotas.colisionando = true;
					}
					
					if (colisionPelotaPaleta(pelotas, jugador1.paleta))
					{
						pelotas.velocidad.y *= -1;
						pelotas.colisionando = true;
					}

					colisionBloquePelota(bloques, pelotas, jugador1.puntaje);
					
				}
				else
				{
					if (!colisionPelotaLadoSup(pelotas))
					{
						if (!colisionPelotaLadoDer(pelotas) && !colisionPelotaLadoIzq(pelotas))
						{							
							if (!colisionPelotaPaleta(pelotas, jugador1.paleta))
							{
								colisionBloquePelota(bloques, pelotas, jugador1.puntaje);
								
							}
							else
							{
								pelotas.colisionando = true;
							}
						}
						else
						{
							pelotas.colisionando = true;
						}						
					}
					else
					{
						pelotas.colisionando = true;
					}
				}
				

				pelotas.posicion.x += pelotas.velocidad.x;
				pelotas.posicion.y += pelotas.velocidad.y;

			}
			else
			{
				inicializar::inicializarPelota(pelotas);
			}
		}

		void setearNivelCompletado()
		{
			if (jugador1.nivel-1 < cantNiveles)
			{
				jugador1.nivelesCompletados[jugador1.nivel - 1] = true;
				jugador1.nivel++;
				inicializar::inicializarPelota(pelota[0]);
			}			
		}
				
		void cargarNivel2()
		{			
			for (short i = 0; i < cantFilasBloques; i++)
			{				
				bloques[i][0].vida = 3;				
				bloques[i][0].tipoDeBloque = TipoBloque::Dificil;				
				
				bloques[i][cantColumnasBloques - 1].vida = 3;
				bloques[i][cantColumnasBloques - 1].tipoDeBloque = TipoBloque::Dificil;

				bloques[i][1].vida = 2;
				bloques[i][1].tipoDeBloque = TipoBloque::Medio;

				bloques[i][cantColumnasBloques - 2].vida = 2;
				bloques[i][cantColumnasBloques - 2].tipoDeBloque = TipoBloque::Medio;

				for (short j = 2; j < cantColumnasBloques-2; j++)
				{					
					bloques[i][j].vida = 1;					
					bloques[i][j].tipoDeBloque = TipoBloque::Facil;					
				}
				
			}
		}

		void cargarNivel3()
		{
			for (short i = 0; i < cantFilasBloques; i++)
			{
				bloques[i][0].vida = 3;
				bloques[i][0].tipoDeBloque = TipoBloque::Dificil;

				bloques[i][cantColumnasBloques - 1].vida = 3;
				bloques[i][cantColumnasBloques - 1].tipoDeBloque = TipoBloque::Dificil;


				for (short j = 1; j < cantColumnasBloques - 1; j++)
				{
					if (i == 0 || i == cantFilasBloques - 1)
					{
						bloques[i][j].vida = 3;
						bloques[i][j].tipoDeBloque = TipoBloque::Dificil;
					}
					else
					{
						if (i == 1 || i == cantFilasBloques - 2 || j == 1 || j == cantColumnasBloques - 2)
						{
							bloques[i][j].vida = 2;
							bloques[i][j].tipoDeBloque = TipoBloque::Medio;
						}
						else
						{
							bloques[i][j].vida = 1;
							bloques[i][j].tipoDeBloque = TipoBloque::Facil;
						}
					}
				}
			}
		}

		void cargarNuevoNivel()
		{
			switch (jugador1.nivel)
			{
			case 2:
				inicializar::inicializarBloques();
				cargarNivel2();
				break;
			case 3:
				inicializar::inicializarBloques();
				cargarNivel3();
				break;
			
			default:
				break;
			}
		}

		void calculoDePuntaje(int & puntaje, TipoBloque tipoDeBloque)
		{
			switch (tipoDeBloque)
			{
			case TipoBloque::Facil:
				puntaje += juego::puntajeBloqueFacil;
				break;
			case TipoBloque::Medio:
				puntaje += juego::puntajeBloqueMedio;
				break;
			case TipoBloque::Dificil:
				puntaje += juego::puntajeBloqueDificil;
				break;
			default:
				break;
			}
		}		
	}

	void init()
	{
		using namespace inicializar;
		inicializarJugador();
		inicializarPelota();
		inicializarBloques();
		inicializarFondo();		
	}

	void update()
	{
		using namespace actualizar;

		musica::gameplay::playMusica();
		musica::gameplay::updateMusica();

		movimientoPelota(pelota[0]);

		imput();

		if (chequeos::chequeoNivelCompletado(bloques))
		{
			setearNivelCompletado();
			if (chequeos::chequeoJuegoFinalizado(jugador1.nivelesCompletados))
			{
				finalizarPartida();
			}
			else
			{
				cargarNuevoNivel();
			}
			

		}

	}

	void draw()
	{
		using namespace dibujar;
		ClearBackground(BLACK);
		dibujarFondo();
		dibujarBloques();
		dibujarPaleta();
		dibujarPelota();
		dibujarPuntaje();
		dibujarNivel();
		dibujarVidas();
	}

	void deinit()
	{
		using namespace desinicializar;
		juegoInicializado = false;
		desinicializarTexturas();
	}

	void jugar()
	{
		if (!juegoInicializado)
		{
			init();
			juegoInicializado = true;
		}
		update();
	}
	
}
