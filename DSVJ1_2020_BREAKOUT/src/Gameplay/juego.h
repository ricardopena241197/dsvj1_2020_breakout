#ifndef JUEGO_H
#define JUEGO_H

#include "raylib.h"
#include "Ventana/ventana.h"
#include "Texturas/texturas.h"

const int cantNiveles = 3;
enum class Poderes
{
	Ninguno
};
enum class TipoBloque
{
	Facil,
	Medio,
	Dificil
};

struct Bloques
{
	Rectangle figura;
	int vida;
	bool activo;
	TipoBloque tipoDeBloque;
	Color color;	
};

struct Pelota
{
	bool enMovimiento;
	bool colisionando;
	Vector2 posicion;
	Vector2 velocidad;
	float radio;
	Color color;
	Texture2D textura;
};

struct Paletas
{
	Rectangle figura;
	int velocidad;
	Color color;
	Texture2D textura;
};

struct Jugador
{
	int vidas;
	int puntaje;
	int nivel;
	bool nivelesCompletados[cantNiveles];
	Paletas paleta;
	
	Poderes poder;
};

struct Fondo
{
	Vector2 pos;
	Texture2D textura;
	Color color;
};

namespace juego
{
	const int cantFilasBloques = 10;
	const int cantColumnasBloques = 5;
	const int cantMaxPelotas = 5;

	const int puntajeBloqueFacil = 100;
	const int puntajeBloqueMedio = 200;
	const int puntajeBloqueDificil = 300;

	namespace paleta
	{
		const int ancho = 50;
		const int alto = 10;
		const int velocidad = 5;
	}

	namespace bloque
	{
		const int ancho = (ventana::anchoVentana/cantColumnasBloques);
		const int alto = ((ventana::altoVentana/2) / cantFilasBloques);
	}

	namespace actualizar
	{
		void calculoDePuntaje(int& puntaje, TipoBloque tipoDeBloque);
	}

	void draw();
	void jugar();
	void deinit();
}

#endif // !JUEGO_H

