#include "menu_creditos.h"
#include "Menus/MenuPrincipal/menu_principal.h"
#include "Controles/controles.h"
#include "AuxGlobales/aux_globales.h"
#include "Texturas/texturas.h"
#include "Breakout/breakout.h"
#include "Texto/texto.h"
#include "Sonidos/musica.h"

namespace menu_creditos
{
	bool menuInicializado = false;

	FondoMenu fondo;
	OpcionesMenu titulo;
	OpcionesMenu opciones[cantOpciones];
	Color colorOpcion = BLACK;

	string creditos;
	Vector2 posCreditos;
	int tamCreditos;

	Vector2 posTitulo;
	Vector2 posOpciones[cantOpciones];
	int iAuxCursor = 0;

	namespace inicializar
	{
		void inicializarFondo()
		{
			fondo.textura = LoadTexture(texturas::menu_principal::rutaFondo);
			fondo.pos.x = 0;
			fondo.pos.y = 0;
			fondo.textura.width = ventana::anchoVentana;
			fondo.textura.height = ventana::altoVentana;
			fondo.tinte = WHITE;
		}

		void inicializarTitulo()
		{
			titulo.pos.y = 10;
			titulo.tamanoTexto = 60;
			titulo.color = colorTituloDefecto;
			titulo.fondo = LoadTexture(texturas::menu_principal::rutaFondoTitulo);
			titulo.tinteFondo = WHITE;
			titulo.texto = "Creditos";
			titulo.tamano = texto::calcularTamTexto(titulo.texto, titulo.tamanoTexto);
			titulo.pos.x = ventana::anchoVentana / 2 - titulo.tamano / 2;
			texturas::menu_principal::setearTamTexturaTitulo(titulo.fondo, titulo);
		}

		void inicializarOpciones()
		{			
			opciones[0].tamanoTexto = 30;
			opciones[0].pos.y = ventana::altoVentana - opciones[0].tamanoTexto*2;
			opciones[0].color = colorOpcionesDefecto;
			opciones[0].fondo = LoadTexture(texturas::menu_principal::rutaFondoOpciones);
			opciones[0].tinteFondo = WHITE;
			opciones[0].texto = "Volver";
			opciones[0].tamano = texto::calcularTamTexto(opciones[0].texto, opciones[0].tamanoTexto);
			opciones[0].pos.x = ventana::anchoVentana / 2 - opciones[0].tamano / 2;
			texturas::menu_principal::setearTamTexturaOpciones(opciones[0].fondo, opciones[0]);			
		}

		void inicializarCreditos()
		{
			posCreditos.y = titulo.pos.y + titulo.tamanoTexto*2;
			posCreditos.x = 2;
			tamCreditos = 30;			
			creditos = "Desarrollo: Ricardo Pena.\nArte: Ricardo Pena, Tim Kaminski y Raphael Lacoste.\nMusica: Nazar Rybak.";
		}
	}

	namespace dibujar
	{
		void dibujarFondo()
		{
			DrawTexture(fondo.textura, fondo.pos.x, fondo.pos.y, fondo.tinte);
		}

		void dibujarTitulo()
		{
			char auxTexto[cantCaracteres];
			strcpy_s(auxTexto, titulo.texto.c_str());
			DrawTexture(titulo.fondo, titulo.pos.x - titulo.tamano / 4, titulo.pos.y, titulo.tinteFondo);
			DrawText(auxTexto, titulo.pos.x, titulo.pos.y, titulo.tamanoTexto, titulo.color);

		}

		void dibujarOpciones()
		{
			using namespace texturas;
			char auxTexto[cantCaracteres];

			for (short i = 0; i < cantOpciones; i++)
			{
				if (iAuxCursor == i)
				{
					opciones[i].color = colorOpcionesAlternativo;
				}
				else
				{
					opciones[i].color = colorOpcionesDefecto;
				}

				strcpy_s(auxTexto, opciones[i].texto.c_str());

				DrawTexture(opciones[i].fondo, opciones[i].pos.x - anchoBordeBotones / 2, opciones[i].pos.y - anchoBordeBotones / 4, opciones[i].tinteFondo);
				DrawText(auxTexto, opciones[i].pos.x, opciones[i].pos.y, opciones[i].tamanoTexto, opciones[i].color);
			}
		}

		void dibujarCreditos()
		{
			using namespace texturas;
			char auxTexto[cantCaracteres];

			strcpy_s(auxTexto, creditos.c_str());			
			DrawText(auxTexto, posCreditos.x, posCreditos.y, tamCreditos, ORANGE);
		}
	}

	void init()
	{
		using namespace inicializar;

		inicializarTitulo();
		inicializarOpciones();
		inicializarFondo();
		inicializarCreditos();

		posTitulo.y = 0;
		posTitulo.x = 2;

		iAuxCursor = 0;
	}

	void update()
	{
		using namespace controles;

		musica::menu::updateMusica();

		if (IsKeyPressed(moverArriba)) { iAuxCursor = (iAuxCursor == 0) ? cantOpciones - 1 : iAuxCursor - 1; }
		if (IsKeyPressed(moverAbajo)) { iAuxCursor = (iAuxCursor == cantOpciones - 1) ? 0 : iAuxCursor + 1; }
		if (IsKeyPressed(aceptar))
		{
			deinit();
			switch ((opcionesMenu)iAuxCursor)
			{			
			case opcionesMenu::Volver:
				breakout::escena = Escenas::MenuPrincipal;
				break;
			default:
				break;
			}
		}
	}

	void draw()
	{
		using namespace dibujar;

		ClearBackground(RAYWHITE);

		dibujarFondo();

		dibujarTitulo();

		dibujarOpciones();

		dibujarCreditos();
	}

	void deinit()
	{
		menuInicializado = false;
		UnloadTexture(titulo.fondo);
		UnloadTexture(opciones[0].fondo);
		UnloadTexture(fondo.textura);
	}

	void menu()
	{
		if (!menuInicializado)
		{
			init();
			menuInicializado = true;
		}

		update();
	}

}