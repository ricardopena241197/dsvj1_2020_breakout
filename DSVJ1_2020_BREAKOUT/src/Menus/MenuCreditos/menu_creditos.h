#ifndef MENU_CREDITOS_H
#define MENU_CREDITOS_H
#include "Ventana/ventana.h"
#include "raylib.h"

#include <iostream>
#include <string.h>

namespace menu_creditos
{
	enum class opcionesMenu { Volver };
	const int espacioEntreOpciones = ventana::altoVentana / 5;
	const int cantOpciones = 1;
	const int cantCaracteres = 200;

	const Color colorTituloDefecto = BLACK;
	const Color colorOpcionesDefecto = BLACK;
	const Color colorOpcionesAlternativo = SKYBLUE;

	void init();
	void update();
	void draw();
	void deinit();
	void menu();
}

#endif // !MENU_CREDITOS_H
