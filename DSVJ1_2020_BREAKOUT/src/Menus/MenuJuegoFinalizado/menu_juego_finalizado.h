#ifndef MENU_JUEGO_FINALIZADO_H
#define MENU_JUEGO_FINALIZADO_H

#include "raylib.h"
#include "Ventana/ventana.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace menu_juego_finalizado
{
	enum class opcionesMenu { JugarDeNuevo, VolverMenu, Salir };
	const int espacioEntreOpciones = ventana::altoVentana / 5;
	const int cantOpciones = 3;
	const int cantCaracteres = 20;

	const Color colorTituloDefecto = ORANGE;
	const Color colorOpcionesDefecto = BLACK;
	const Color colorOpcionesAlternativo = SKYBLUE;

	void init();
	void update();
	void draw();
	void deinit();
	void menu();
}
#endif // !MENU_JUEGO_FINALIZADO_H

