#include "menu_opciones.h"
#include "Menus/MenuPrincipal/menu_principal.h"
#include "Controles/controles.h"
#include "AuxGlobales/aux_globales.h"
#include "Texturas/texturas.h"
#include "Breakout/breakout.h"
#include "Texto/texto.h"
#include "Sonidos/musica.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace menu_opciones
{
	bool menuInicializado = false;

	FondoMenu fondo;

	OpcionesMenu titulo;
	OpcionesMenu opciones[cantOpciones];
	Color colorOpcion = BLACK;

	Vector2 posTitulo;
	Vector2 posOpciones[cantOpciones];
	int iAuxCursor = 0;

	namespace inicializar
	{
		void inicializarFondo()
		{
			fondo.textura = LoadTexture(texturas::menu_principal::rutaFondo);
			fondo.pos.x = 0;
			fondo.pos.y = 0;
			fondo.textura.width = ventana::anchoVentana;
			fondo.textura.height = ventana::altoVentana;
			fondo.tinte = WHITE;
		}

		void inicializarTitulo()
		{
			titulo.pos.y = 10;
			titulo.tamanoTexto = 60;
			titulo.color = colorTituloDefecto;
			titulo.fondo = LoadTexture(texturas::menu_principal::rutaFondoTitulo);
			titulo.tinteFondo = WHITE;
			titulo.texto = "Opciones";
			titulo.tamano = texto::calcularTamTexto(titulo.texto, titulo.tamanoTexto);
			titulo.pos.x = ventana::anchoVentana / 2 - titulo.tamano / 2;
			texturas::menu_principal::setearTamTexturaTitulo(titulo.fondo, titulo);
		}

		void inicializarOpciones()
		{
			opciones[0].pos.y = ventana::altoVentana / 5;
			opciones[0].tamanoTexto = 30;
			opciones[0].color = colorOpcionesDefecto;
			opciones[0].fondo = LoadTexture(texturas::menu_principal::rutaFondoOpciones);
			opciones[0].tinteFondo = WHITE;
			opciones[0].texto = "Subir Volumen";
			opciones[0].tamano = texto::calcularTamTexto(opciones[0].texto, opciones[0].tamanoTexto);
			opciones[0].pos.x = ventana::anchoVentana / 2 - opciones[0].tamano / 2;
			texturas::menu_principal::setearTamTexturaOpciones(opciones[0].fondo, opciones[0]);

			for (short i = 1; i < cantOpciones; i++)
			{
				opciones[i].pos.y = opciones[i - 1].pos.y + espacioEntreOpciones;
				opciones[i].tamanoTexto = 30;
				opciones[i].color = colorOpcionesDefecto;
				opciones[i].fondo = opciones[i-1].fondo;//LoadTexture(texturas::menu_principal::rutaFondoOpciones);
				opciones[i].tinteFondo = WHITE;

				switch (i)
				{
				case 1:
					opciones[i].texto = "Bajar Volumen";
					break;
				case 2:					
					opciones[i].texto = "Volver";
					break;

				default:
					break;
				}
				opciones[i].tamano = texto::calcularTamTexto(opciones[i].texto, opciones[i].tamanoTexto);

				opciones[i].pos.x = ventana::anchoVentana / 2 - opciones[i].tamano / 2;
				texturas::menu_principal::setearTamTexturaOpciones(opciones[i].fondo, opciones[i]);
			}
		}
	}

	namespace dibujar
	{
		void dibujarFondo()
		{
			DrawTexture(fondo.textura, fondo.pos.x, fondo.pos.y, fondo.tinte);
		}

		void dibujarTitulo()
		{
			char auxTexto[cantCaracteres];
			strcpy_s(auxTexto, titulo.texto.c_str());
			DrawTexture(titulo.fondo, titulo.pos.x - titulo.tamano / 4, titulo.pos.y, titulo.tinteFondo);
			DrawText(auxTexto, titulo.pos.x, titulo.pos.y, titulo.tamanoTexto, titulo.color);

		}

		void dibujarOpciones()
		{
			using namespace texturas;
			char auxTexto[cantCaracteres];

			for (short i = 0; i < cantOpciones; i++)
			{
				if (iAuxCursor == i)
				{
					opciones[i].color = colorOpcionesAlternativo;
				}
				else
				{
					opciones[i].color = colorOpcionesDefecto;
				}

				strcpy_s(auxTexto, opciones[i].texto.c_str());

				DrawTexture(opciones[i].fondo, opciones[i].pos.x - anchoBordeBotones / 2, opciones[i].pos.y - anchoBordeBotones / 4, opciones[i].tinteFondo);
				DrawText(auxTexto, opciones[i].pos.x, opciones[i].pos.y, opciones[i].tamanoTexto, opciones[i].color);
			}
		}
	}

	void init()
	{
		using namespace inicializar;

		inicializarTitulo();
		inicializarOpciones();
		inicializarFondo();

		posTitulo.y = 0;
		posTitulo.x = 2;

		iAuxCursor = 0;

	}

	void update()
	{
		using namespace controles;		
		musica::menu::updateMusica();
		
		if (IsKeyPressed(moverArriba)) { iAuxCursor = (iAuxCursor == 0) ? cantOpciones - 1 : iAuxCursor - 1; }
		if (IsKeyPressed(moverAbajo)) { iAuxCursor = (iAuxCursor == cantOpciones - 1) ? 0 : iAuxCursor + 1; }
		if (IsKeyPressed(aceptar))
		{
			using namespace musica;			
			switch ((opcionesMenu)iAuxCursor)
			{
			case opcionesMenu::SubirMusicaMenu:

				menu::volumen = (menu::volumen<1.0f) ? menu::volumen+0.1 : menu::volumen;
				SetMusicVolume(menu::musica, menu::volumen);

				gameplay::volumen = (gameplay::volumen < 1.0f) ? gameplay::volumen + 0.1 : gameplay::volumen;
				SetMusicVolume(gameplay::musica, gameplay::volumen);
				break;

			case opcionesMenu::BajarMusicaMenu:
				menu::volumen =( menu::volumen > 0.1f) ? menu::volumen - 0.1 : menu::volumen;
				SetMusicVolume(menu::musica, menu::volumen);

				gameplay::volumen = (gameplay::volumen > 0.1f) ? gameplay::volumen - 0.1 : gameplay::volumen;
				SetMusicVolume(gameplay::musica, gameplay::volumen);
				break;
			
			case opcionesMenu::Volver:
				breakout::escena = Escenas::MenuPrincipal;				
				break;
			default:
				break;
			}

		}
	}

	void draw()
	{
		using namespace dibujar;

		ClearBackground(RAYWHITE);

		dibujarFondo();

		dibujarTitulo();

		dibujarOpciones();

	}

	void deinit()
	{
		menuInicializado = false;
		UnloadTexture(titulo.fondo);
		UnloadTexture(opciones[0].fondo);
		UnloadTexture(fondo.textura);
	}

	void menu()
	{
		if (!menuInicializado)
		{
			init();
			menuInicializado = true;
		}
		update();
	}
}