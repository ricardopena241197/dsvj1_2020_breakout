#ifndef MENU_OPCIONES_H
#define MENU_OPCIONES_H

#include "raylib.h"
#include "Ventana/ventana.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace menu_opciones
{
	enum class opcionesMenu { SubirMusicaMenu, BajarMusicaMenu, Volver };
	const int espacioEntreOpciones = ventana::altoVentana / 6;
	const int cantOpciones = 3;
	const int cantCaracteres = 30;

	const Color colorTituloDefecto = BLACK;
	const Color colorOpcionesDefecto = BLACK;
	const Color colorOpcionesAlternativo = SKYBLUE;

	void init();
	void update();
	void draw();
	void deinit();
	void menu();
}

#endif // !MENU_OPCIONES_H

