#ifndef MENU_PAUSA
#define MENU_PAUSA

#include "raylib.h"
#include "Ventana/ventana.h"

#include <iostream>
#include <string.h>

using namespace std;

namespace menu_pausa
{
	enum class opcionesMenu { Vover, IrMenu, Salir };
	const int espacioEntreOpciones = ventana::altoVentana / 5;
	const int cantOpciones = 3;
	const int cantCaracteres = 20;

	const Color colorTituloDefecto = ORANGE;
	const Color colorOpcionesDefecto = BLACK;
	const Color colorOpcionesAlternativo = SKYBLUE;

	void init();
	void update();
	void draw();
	void deinit();
	void menu();
}


#endif // !MENU_PAUSA

