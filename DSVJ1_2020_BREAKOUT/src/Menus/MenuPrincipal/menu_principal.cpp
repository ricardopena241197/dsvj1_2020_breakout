#include "menu_principal.h"
#include "Controles/controles.h"
#include "AuxGlobales/aux_globales.h"
#include "Texturas/texturas.h"
#include "Breakout/breakout.h"
#include "Texto/texto.h"
#include "Sonidos/musica.h"

namespace menu_principal
{
	bool menuInicializado = false;

	FondoMenu fondo;

	OpcionesMenu titulo;
	OpcionesMenu opciones[cantOpciones];
	Color colorOpcion = BLACK;

	Vector2 posTitulo;
	Vector2 posOpciones[cantOpciones];
	int iAuxCursor = 0;

	namespace inicializar
	{
		void inicializarFondo()
		{
			fondo.textura = LoadTexture(texturas::menu_principal::rutaFondo);
			fondo.pos.x = 0;
			fondo.pos.y = 0;
			fondo.textura.width = ventana::anchoVentana;
			fondo.textura.height = ventana::altoVentana;
			fondo.tinte = WHITE;
		}

		void inicializarTitulo()
		{			
			titulo.pos.y = 10;
			titulo.tamanoTexto = 60;			
			titulo.color = colorTituloDefecto;
			titulo.fondo = LoadTexture(texturas::menu_principal::rutaFondoTitulo);
			titulo.tinteFondo = WHITE;
			titulo.texto = "VikingBreakeR";
			titulo.tamano = texto::calcularTamTexto(titulo.texto,titulo.tamanoTexto);
			titulo.pos.x = ventana::anchoVentana/2-titulo.tamano/2;
			texturas::menu_principal::setearTamTexturaTitulo(titulo.fondo,titulo);
		}

		void inicializarOpciones()
		{			
			opciones[0].pos.y = ventana::altoVentana / 4;
			opciones[0].tamanoTexto = 30;
			opciones[0].color = colorOpcionesDefecto;
			opciones[0].fondo = LoadTexture(texturas::menu_principal::rutaFondoOpciones);
			opciones[0].tinteFondo = WHITE;
			opciones[0].texto = "Jugar";
			opciones[0].tamano= texto::calcularTamTexto(opciones[0].texto, opciones[0].tamanoTexto);
			opciones[0].pos.x = ventana::anchoVentana / 2 - opciones[0].tamano / 2; 
			texturas::menu_principal::setearTamTexturaOpciones(opciones[0].fondo, opciones[0]);

			for (short i = 1; i < cantOpciones; i++)
			{				
				opciones[i].pos.y = opciones[i - 1].pos.y + espacioEntreOpciones;
				opciones[i].tamanoTexto = 30;
				opciones[i].color = colorOpcionesDefecto;
				opciones[i].fondo = opciones[i-1].fondo;
				opciones[i].tinteFondo = WHITE;			

				switch (i)
				{
				case 1:
					opciones[i].texto = "Opciones";
					break;
				case 2:
					opciones[i].texto = "Creditos";
					break;
				case 3:
					opciones[i].texto = "Salir";
					break;

				default:
					break;
				}
				opciones[i].tamano = texto::calcularTamTexto(opciones[i].texto, opciones[i].tamanoTexto);

				opciones[i].pos.x = ventana::anchoVentana / 2 - opciones[i].tamano / 2;
				texturas::menu_principal::setearTamTexturaOpciones(opciones[i].fondo, opciones[i]);
			}
		}		
	}

	namespace dibujar
	{
		void dibujarFondo()
		{
			DrawTexture(fondo.textura, fondo.pos.x, fondo.pos.y, fondo.tinte);
		}

		void dibujarTitulo()
		{
			char auxTexto[cantCaracteres];
			strcpy_s(auxTexto, titulo.texto.c_str());
			DrawTexture(titulo.fondo,titulo.pos.x - titulo.tamano/4, titulo.pos.y, titulo.tinteFondo);
			DrawText(auxTexto, titulo.pos.x, titulo.pos.y, titulo.tamanoTexto, titulo.color);
			
		}

		void dibujarOpciones()
		{
			using namespace texturas;
			char auxTexto[cantCaracteres];

			for (short i = 0; i < cantOpciones; i++)
			{
				if (iAuxCursor == i)
				{
					opciones[i].color = colorOpcionesAlternativo;
				}
				else
				{
					opciones[i].color = colorOpcionesDefecto;
				}

				strcpy_s(auxTexto, opciones[i].texto.c_str());				

				DrawTexture(opciones[i].fondo, opciones[i].pos.x-anchoBordeBotones/2, opciones[i].pos.y-anchoBordeBotones / 4, opciones[i].tinteFondo);
				DrawText(auxTexto, opciones[i].pos.x, opciones[i].pos.y, opciones[i].tamanoTexto,opciones[i].color);
			}
		}
	}

	void init()
	{			
		using namespace inicializar;

		inicializarTitulo();
		inicializarOpciones();
		inicializarFondo();

		posTitulo.y = 0;
		posTitulo.x = 2;		

		iAuxCursor = 0;		
	}

	void update()
	{		
		using namespace controles;

		musica::menu::playMusica();
		musica::menu::updateMusica();

		if (IsKeyPressed(moverArriba)) { iAuxCursor = (iAuxCursor == 0) ? cantOpciones - 1 : iAuxCursor - 1; }
		if (IsKeyPressed(moverAbajo)) { iAuxCursor = (iAuxCursor == cantOpciones - 1) ? 0 : iAuxCursor + 1; }
		if (IsKeyPressed(aceptar))
		{			
			deinit();
			switch ((opcionesMenu)iAuxCursor)
			{
			case opcionesMenu::Jugar:
				breakout::escena = Escenas::Jugar;
				break;
			case opcionesMenu::Opciones:
				breakout::escena = Escenas::Opciones;
				break;
			case opcionesMenu::Creditos:
				breakout::escena = Escenas::Creditos;
				break;
			case opcionesMenu::Salir:
				aux_globales::aux_salida = true;
				break;
			default:
				break;
			}

		}
	}

	void draw()
	{
		using namespace dibujar;

		ClearBackground(RAYWHITE);

		dibujarFondo();

		dibujarTitulo();

		dibujarOpciones();
		
	}

	void deinit()
	{
		menuInicializado = false;
		menuInicializado = false;
		UnloadTexture(titulo.fondo);
		UnloadTexture(opciones[0].fondo);
		UnloadTexture(fondo.textura);
	}

	void menu()
	{
		if (!menuInicializado)
		{
			init();
			menuInicializado = true;
		}
		update();
	}
}