#ifndef MENU_PRINCIPAL_H
#define MENU_PRINCIPAL_H
#include "raylib.h"
#include "Ventana/ventana.h"

#include <iostream>
#include <string.h>

using namespace std;

struct OpcionesMenu
{
	Vector2 pos;
	int tamanoTexto;
	int tamano;
	Color color;
	string texto;
	Texture2D fondo;
	Color tinteFondo;
};

struct FondoMenu
{
	Vector2 pos;	
	Texture2D textura;
	Color tinte;
};

namespace menu_principal
{
	enum class opcionesMenu { Jugar, Opciones, Creditos, Salir };
	const int espacioEntreOpciones = ventana::altoVentana/5;
	const int cantOpciones = 4;	
	const int cantCaracteres = 20;	

	const Color colorTituloDefecto = BLACK;
	const Color colorOpcionesDefecto = BLACK;
	const Color colorOpcionesAlternativo = SKYBLUE;

	void init();
	void update();
	void draw();
	void deinit();
	void menu();
}

#endif // !MENU_PRINCIPAL_H

