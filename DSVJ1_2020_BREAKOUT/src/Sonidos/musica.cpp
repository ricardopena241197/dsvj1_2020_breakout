#include "musica.h"

namespace musica
{
	namespace menu
	{
		Music musica;
		float volumen;
		void playMusica()
		{
			PlayMusicStream(musica);
		}

		void updateMusica()
		{
			UpdateMusicStream(musica);
		}
	}

	namespace gameplay
	{
		Music musica;
		float volumen;
		void playMusica()
		{
			PlayMusicStream(musica);
		}

		void updateMusica()
		{
			UpdateMusicStream(musica);
		}
	}

	void init()
	{
		menu::musica = LoadMusicStream(menu::rutaMusica);
		menu::volumen = 0.5f;
		SetMusicVolume(menu::musica, menu::volumen);

		gameplay::musica = LoadMusicStream(gameplay::rutaMusica);
		gameplay::volumen = 0.5f;
		SetMusicVolume(gameplay::musica, menu::volumen);
	}

	

	

	

	void deinit()
	{
		UnloadMusicStream(menu::musica);
		UnloadMusicStream(gameplay::musica);
	}

}