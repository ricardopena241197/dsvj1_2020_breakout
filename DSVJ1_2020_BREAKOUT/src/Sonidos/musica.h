#ifndef MUSICA_H
#define MUSICA_H
#include "raylib.h"

namespace musica
{
	namespace menu
	{
		extern Music musica;
		extern float volumen;
		const char rutaMusica[] = "res/assets/sounds/menu/musica.mp3";

		extern void playMusica();
		extern void updateMusica();
	}
	
	namespace gameplay
	{
		extern Music musica;
		extern float volumen;
		const char rutaMusica[] = "res/assets/sounds/gameplay/musica.mp3";

		extern void playMusica();
		extern void updateMusica();
	}

	extern void init();
	extern void deinit();
}

#endif // !MUSICA_H

