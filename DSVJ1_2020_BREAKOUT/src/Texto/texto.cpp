#include "texto.h"

namespace texto
{
	int calcularTamTexto(string texto, int tamTexto)
	{
		using namespace menu_principal;
		char auxTexto[cantCaracteres];

		strcpy_s(auxTexto, texto.c_str());

		return MeasureText(auxTexto, tamTexto);
	}
}
