#ifndef TEXTO_H
#define TEXTO_H

#include "Menus/MenuPrincipal/menu_principal.h"

#include <iostream>
#include <string.h>


using namespace std;

namespace texto
{
	int calcularTamTexto(string texto, int tamTexto);
}

#endif // !TEXTO_H

