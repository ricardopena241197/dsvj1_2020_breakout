#include "texturas.h"

namespace texturas
{	
	namespace menu_principal
	{
		void setearTamTexturaTitulo(Texture2D& textura, OpcionesMenu opcion)
		{
			textura.height = opcion.tamanoTexto;
			textura.width = opcion.tamano + opcion.tamano / 2;
		}

		void setearTamTexturaOpciones(Texture2D& textura, OpcionesMenu opcion)
		{
			textura.height = opcion.tamanoTexto + anchoBordeBotones / 2;
			textura.width = opcion.tamano + anchoBordeBotones;
		}
	}
	
}