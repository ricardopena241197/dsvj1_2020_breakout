#ifndef TEXTURAS_H
#define TEXTURAS_H

#include "raylib.h"
#include "Menus/MenuPrincipal/menu_principal.h"

const int anchoBordeBotones = 60;

namespace texturas
{
	namespace juego
	{
		const char rutaFondo[] = "res/assets/textures/gameplay/fondo.png";
		const char rutaPaleta[] = "res/assets/textures/gameplay/paleta.png";
		const char rutaPelota[] = "res/assets/textures/gameplay/pelota.png";
		const char rutaVidas[] = "res/assets/textures/gameplay/vidas.png";

		const char rutaBloqueFacil[] = "res/assets/textures/gameplay/bloque_facil.png";
		const char rutaBloqueMedio[] = "res/assets/textures/gameplay/bloque_medio.png";
		const char rutaBloqueDificil[] = "res/assets/textures/gameplay/bloque_dificil.png";
	}

	namespace menu_principal
	{		
		const char rutaFondo[] = "res/assets/textures/menu_principal/fondo.png";
		const char rutaFondoTitulo[] = "res/assets/textures/menu_principal/fondo_titulo.png";
		const char rutaFondoOpciones[] = "res/assets/textures/menu_principal/fondo_opciones.png";

		void setearTamTexturaTitulo(Texture2D& textura, OpcionesMenu opcion);
		void setearTamTexturaOpciones(Texture2D& textura, OpcionesMenu opcion);
	}

	
}

#endif // !TEXTURAS_H

