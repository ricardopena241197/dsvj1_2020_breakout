#include "ventana.h"
#include "raylib.h"

namespace ventana
{
	void abrirVentana()
	{
		SetTargetFPS(fps);
		InitWindow(anchoVentana, altoVentana, "Breakout by RPP");
		SetExitKey(KEY_F1);
	}
}