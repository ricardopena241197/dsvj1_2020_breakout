#ifndef VENTANA_H
#define VENTANA_H

namespace ventana
{
	const int altoVentana = 500;
	const int anchoVentana = 800;

	const int fps = 60;

	void abrirVentana();
}

#endif // !VENTANA_H
